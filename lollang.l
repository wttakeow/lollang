%option noyywrap

%{
#include <stdio.h>
#include "lollang.tab.h"

int bin2dec(char* bin); /* convert binary to decimal */
int hex2dec(char* constant); /* convert heximal to decimal */
/* Token list 
	[\|][any char][\|] => string token
	[0-9]+ => decimal number token 
	("0b"|"0B")[0-1]+ => binary token
	("0h"|"0H")[0-9a-fA-F]+ => heximal token
	("#reg_")[a-z] => register token

	Operators token are
	"+" 
	"-" 
	"*" 
	"%" 
	"/" 
	"(" 
	")" 


	"is" => assign token
	"show" => print string token
	"show_h" => print in hex token

	"=?" => check if equal token
	"if" => if token
	"loop" => loop token

	"{" => { token
	"}" => } token
	"to" => to token

	
	"\n" => NEWLINE token

*/
%}

%option yylineno



%%


[\|][ 0-9a-zA-Z_!@#$%^&*()-=+:;*/\\n]+[\|]  {yylval = yytext; return STR;}
[0-9]+					{yylval = atoi(yytext); return DEC;} 
("0b"|"0B")[0-1]+		{yylval = bin2dec(yytext); return BIN;} 
("0h"|"0H")[0-9a-fA-F]+ {yylval = hex2dec(yytext); return HEX;} 
("#reg_")[a-z] {
  yylval=(int)yytext[5]-'a'+1;
  return VAR;
} 

"+" { return plus; }
"-" { return minus; }
"*" { return mul; }
"%" { return mod; }
"/" { return divide; }
"(" { return '('; }
")" { return ')'; }

"is" { return '='; } 
"show" { return SHOW; } 
"show_h" { return SHOWHEX; } 


"=?" { return EQ; } 
"if" { return IF; }
"loop" { return LOOP; }


"{" { return '{'; }
"}" { return '}'; }
"to" { return TO; }

	
"\n"	{ return NEWLINE; }

[ \t]	{ }
.		{ return ERR; }

%%

int bin2dec(char* constant){
	char str[16];
	sprintf(str, "%s", constant+2);
	return strtol(str, NULL, 2);
}

int hex2dec(char* constant){
	char str[16];
	sprintf(str, "%s", constant+2);
	return strtol(str, NULL, 16);
}
