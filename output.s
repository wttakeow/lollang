	.global main
	.text

main:
	mov %rsp,%rbp
	sub $208,%rsp

	mov $1, %rax
	push %rax

	pop %rax
	mov %rax,-8(%rbp)

	mov $1, %rax
	push %rax

	pop %rax
	mov %rax,-16(%rbp)

	mov $0, %rax
	push %rax

	pop %rax
	mov %rax,-24(%rbp)

	mov $0, %rax
	push %rax

	pop %rax
	mov %rax,-32(%rbp)

	mov $2, %rax
	push %rax

	mov $99, %rax
	push %rax

	pop %rcx
	pop %rbx
	pop %rax
	sub %rbx,%rax
	inc %rax
	push %rax
	xor %rax, %rax
	cmp %rax, %rcx
	je endloop0

loop0:
	push %rcx
	mov $1, %rax
	push %rax

	mov -8(%rbp), %rbx
	pop %rax
	add %rbx, %rax
	push %rax

	pop %rax
	mov %rax,-8(%rbp)

	mov $1, %rax
	push %rax

	pop %rax
	mov %rax,-16(%rbp)

	mov $0, %rax
	push %rax

	pop %rax
	mov %rax,-24(%rbp)

	mov $0, %rax
	push %rax

	pop %rax
	mov %rax,-32(%rbp)

	mov $1, %rax
	push %rax

	mov $99, %rax
	push %rax

	pop %rcx
	pop %rbx
	pop %rax
	sub %rbx,%rax
	inc %rax
	push %rax
	xor %rax, %rax
	cmp %rax, %rcx
	je endloop1

loop1:
	push %rcx
	mov $1, %rax
	push %rax

	mov -16(%rbp), %rbx
	pop %rax
	add %rbx, %rax
	push %rax

	pop %rax
	mov %rax,-16(%rbp)

	mov -16(%rbp), %rbx
	mov -8(%rbp), %rax
	cmp %rax, %rbx
	jnz jump0

	mov $1, %rax
	push %rax

	pop %rax
	mov %rax,-32(%rbp)

jump0:
	mov $0, %rax
	push %rax

	mov -32(%rbp), %rbx
	pop %rax
	cmp %rax, %rbx
	jnz jump1

	mov -16(%rbp), %rbx
	mov -8(%rbp), %rax
	xor %rdx, %rdx
	idiv %rbx
	push %rdx

	mov $0, %rax
	push %rax

	pop %rbx
	pop %rax
	cmp %rax, %rbx
	jnz jump1

	mov $1, %rax
	push %rax

	pop %rax
	mov %rax,-24(%rbp)

jump1:
jump2:
	pop %rcx
	dec %rcx
	jnz loop1

endloop1:
	mov $0, %rax
	push %rax

	mov -24(%rbp), %rbx
	pop %rax
	cmp %rax, %rbx
	jnz jump3

	push %rax
	push %rbx
	push %rcx
	mov $print, %rdi
	mov -8(%rbp), %rax
	mov %rax, %rsi
	xor %rax, %rax
	call printf
	pop %rcx
	pop %rbx
	pop %rax

jump3:
	pop %rcx
	dec %rcx
	jnz loop0

endloop0:
	add $208, %rsp
	ret

print:
	.asciz "%d\n"
printx:
	.asciz "0x%x\n"
