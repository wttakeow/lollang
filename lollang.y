%{
	#include <math.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include "functions.c"
	
	#define OFFSET 8	

	void yyerror(const char* s);

	extern FILE *yyin;
	extern int yylineno;	//No. line of code
	char* content="";		//for keeping code
	char* strings="";		//for keeping strings 
	char tmpcontent1[100];  //Temporary array for first operands
	char tmpcontent2[100];  //Temporary array for seconds operands
	int jumpPoint = 0;		//pointer for jump
	int loopPoint = 0;		//pointer for loop
	int strOrder = 0;       //order of strings

%}


%token DEC BIN HEX STR
%token VAR '='
%token SHOW SHOWHEX
%token EQ IF LOOP '{' '}' TO
%token ERR NEWLINE


%left '(' ')'
%left plus minus mul divide mod

%start Input

%%

Input:
    | Start Input
;

Start:
	  VAR '=' EX NEWLINE {
		content = assigner(content, tmpcontent1, OFFSET*$1);
			tmpcontent1[0]='\0';
		}
	| Condition NEWLINE { }
	| SHOW EX NEWLINE {
		content = printer(content, tmpcontent1, false);
		tmpcontent1[0]='\0';
		}
	| SHOWHEX EX NEWLINE {
		content = printer(content, tmpcontent1, true);
		tmpcontent1[0]='\0';
		}
	| SHOW STR NEWLINE {
		content = printAstr(content, strOrder);
		sprintf(tmpcontent1, "%s", $2);
		strings = createStrings(strings, tmpcontent1, strOrder++);
		tmpcontent1[0]='\0';
		}
	| NEWLINE { }
	| ERR { yyerror("Abort\n"); }
;

EX:
      CONS { }
	| '(' EX ')' { }
	
	| EX mul EX {
			content = multiplier(content, tmpcontent1, tmpcontent2);
			tmpcontent1[0]='\0';
			tmpcontent2[0]='\0';
			$$ = $1*$3;
		}
	| EX mod EX {
			if($3!=0){
			content = modder(content, tmpcontent2, tmpcontent1);
			tmpcontent1[0]='\0';
			tmpcontent2[0]='\0';
			$$ = $1%$3;
			}
			else
			{
				yyerror("Mod by zero");
			}
	}
	| EX divide EX {
			if($3!=0){
				content = divider(content, tmpcontent2, tmpcontent1);
				tmpcontent1[0]='\0';
				tmpcontent2[0]='\0';
				$$ = $1/$3;
			}
			else{
				yyerror("Divided by zero");
			}
		}
	| EX minus EX {
			content = subber(content, tmpcontent2, tmpcontent1);
			tmpcontent1[0]='\0';
			tmpcontent2[0]='\0';
			$$ = $1-$3;
		}
	| EX plus EX {
			content = adder(content, tmpcontent1, tmpcontent2);
			tmpcontent1[0]='\0';
			tmpcontent2[0]='\0';
			$$ = $1+$3;
		}
    

;

Bool:
	| EX EQ EX {
			content = conditioner(content, tmpcontent1, tmpcontent2, jumpPoint);
			printf("AAAA %d line %dAAAA\n", jumpPoint,yylineno);
			tmpcontent1[0]='\0';
			tmpcontent2[0]='\0';
		}
;

Condition:
	  IF '(' Bool ')' '{' NEWLINE Statements '}' {
			content = ifbuilder(content, jumpPoint);
			jumpPoint = jumpPoint+1;
			
		}
	|   IF '(' Bool {
		yyerror("forgot )");
	}

	|   IF Bool ')' {
		yyerror("forgot (");
	}

	|  IF '(' EX ')''{' {
		yyerror("NOT A BOOLEAN CASE");
	}
	| LOOP  LOOPNUM ')' {

			yyerror("forgot (");
	}
	| LOOP  '(' LOOPNUM  {

			yyerror("forgot )");
	}

	| LOOP '(' LOOPNUM ')' '{' NEWLINE Statements '}' {

			content = looptailbuilder(content, --loopPoint);
			
		}
;

Statements:
	Start { }
	| Start Statements { }
;



			

LOOPNUM:
	CONS {
			content = loopbuilder(content, tmpcontent1, loopPoint++);
			tmpcontent1[0]='\0';
		}
	| CONS TO CONS{

		if($3>$1)
		{
			content = loopTObuilder(content, tmpcontent1, loopPoint++);
			tmpcontent1[0]='\0';
		}
		else
		{
			yyerror("NEGATIVE LOOP");
		}
	}

;

CONS:
	  DEC {
		content = constantbuilder(content, $1);
		}
	| minus DEC {
		content = constantbuilder(content, -$2);
		}
	| BIN {
		content = constantbuilder(content, $1);
		}
	| HEX {
		content = constantbuilder(content, $1);
		}
	| VAR {
		if(tmpcontent1[0] == '\0') sprintf(tmpcontent1, "%d", $1*OFFSET);
		else if(tmpcontent2[0] == '\0') sprintf(tmpcontent2, "%d", $1*OFFSET);
		}
;

%%



// Error handler
void yyerror(const char* s) {
	fprintf(stderr, " !!!!  @ line no. %d Error: %s  !!!!\n", yylineno, s);
	exit(1);
}

void main(int argc, const char *argv[]) {
    printf(" Input filename : %s\n", argv[1]); //input file
    yyin = fopen(argv[1], "r");
    if(yyin == NULL){
        printf(" ERROR! File not found.\n");
        exit(1);
    }

    do {
		yyparse();
	} while (!feof(yyin));
	fclose(yyin);
	
	FILE *fp = fopen("output.s","wb");

	createHeader(fp);
	createContent(fp, content);
	createFooter(fp);
	createContent(fp, strings);


	printf("compiled successfully :D\n");
	


	fclose(fp);

	printf("Result: \n"); 
	//call system to compile assembly file from output
	system("gcc output.s -o run");
	system("./run");

}
