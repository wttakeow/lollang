#include <stdbool.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* appendContent(char* content, char* append) {
	char* output = malloc(strlen(content) + strlen(append) + 1);
	strcpy(output, content);
	strcat(output, append);
	return output;
}

void createHeader(FILE *fp) {
	fputs("\t.global main\n", fp);
	fputs("\t.text\n\n", fp);

	fputs("main:\n", fp);
	fputs("\tmov %rsp,%rbp\n", fp);
	fputs("\tsub $208,%rsp\n\n", fp);	// Allocate 26*4 slot for $a-$z
}

void createFooter(FILE *fp) {
	fputs("\tadd $208, %rsp\n", fp);
	fputs("\tret\n\n", fp);

	fputs("print:\n", fp);
	fputs("\t.asciz \"%d\\n\"\n", fp);
	fputs("printx:\n", fp);
	fputs("\t.asciz \"0x%x\\n\"\n", fp);
}


char* createStrings(char* strings, char* strValue, int strOrder) {
	char tmpcontent[100];
	strValue[0] = '\"';
	strValue[strlen(strValue)-2] = '\"'; 

	sprintf(tmpcontent, "str%d:\n", strOrder);
	strings=appendContent(strings, tmpcontent);
	sprintf(tmpcontent, "\t.asciz %s", strValue);
	strings=appendContent(strings, tmpcontent);
	return strings;
}

void createContent(FILE *fp, char* content) {
	
	puts(content);
	fputs(content, fp);
}




char* assigner(char *content, char* op1, int offset) {
	char tmpcontent[100];
	if(op1[0]=='\0') {
		sprintf(tmpcontent, "\tpop %%rax\n");
		content=appendContent(content, tmpcontent);
		sprintf(tmpcontent, "\tmov %%rax,-%d(%%rbp)\n\n", offset);
		content=appendContent(content, tmpcontent);
	}
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rax\n", atoi(op1));
		content=appendContent(content, tmpcontent);
		sprintf(tmpcontent, "\tmov %%rax, -%d(%%rbp)\n\n", offset);
		content=appendContent(content, tmpcontent);
	}
	return content;
}

char* constantbuilder(char *content, int val) {
		char tmpcontent[100];
		sprintf(tmpcontent, "\tmov $%d, %%rax\n",val);
		content=appendContent(content, tmpcontent);
		content=appendContent(content, "\tpush %rax\n\n");
		return content;
}

char* adder(char *content, char* op1, char* op2) {
	char tmpcontent[100];
	if(op1[0] == '\0')
		content=appendContent(content, "\tpop %rbx\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rbx\n", atoi(op1));
		content=appendContent(content, tmpcontent);
	}

	if(op2[0] == '\0')
		content=appendContent(content, "\tpop %rax\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rax\n", atoi(op2));
		content=appendContent(content, tmpcontent);
	}

	content=appendContent(content, "\tadd %rbx, %rax\n");
	content=appendContent(content, "\tpush %rax\n\n");
	return content;
}

char* subber(char *content, char* op1, char* op2) {
	char tmpcontent[100];
	if(op1[0] == '\0')
		content=appendContent(content, "\tpop %rbx\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rbx\n", atoi(op1));
		content=appendContent(content, tmpcontent);
	}

	if(op2[0] == '\0')
		content=appendContent(content, "\tpop %rax\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rax\n", atoi(op2));
		content=appendContent(content, tmpcontent);
	}

	content=appendContent(content, "\tsub %rbx, %rax\n");
	content=appendContent(content, "\tpush %rax\n\n");
	return content;
}

char* multiplier(char *content, char* op1, char* op2) {
	char tmpcontent[100];
	if(op1[0] == '\0')
		content=appendContent(content, "\tpop %rbx\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rbx\n", atoi(op1));
		content=appendContent(content, tmpcontent);
	}

	if(op2[0] == '\0')
		content=appendContent(content, "\tpop %rax\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rax\n", atoi(op2));
		content=appendContent(content, tmpcontent);
	}

	content=appendContent(content, "\tmul %rbx\n");
	content=appendContent(content, "\tpush %rax\n\n");
	return content;
}

char* divider(char *content, char* op1, char* op2) {
	char tmpcontent[100];
	if(op1[0] == '\0')
		content=appendContent(content, "\tpop %rbx\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rbx\n", atoi(op1));
		content=appendContent(content, tmpcontent);
	}

	if(op2[0] == '\0')
		content=appendContent(content, "\tpop %rax\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rax\n", atoi(op2));
		content=appendContent(content, tmpcontent);
	}

	content=appendContent(content, "\txor %rdx, %rdx\n");
	content=appendContent(content, "\tidiv %rbx\n");
	content=appendContent(content, "\tpush %rax\n\n");
	return content;
}

char* modder(char* content, char* op1, char* op2) {
	char tmpcontent[100];
	if(op1[0] == '\0')
		content=appendContent(content, "\tpop %rbx\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rbx\n", atoi(op1));
		content=appendContent(content, tmpcontent);
	}

	if(op2[0] == '\0')
		content=appendContent(content, "\tpop %rax\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rax\n", atoi(op2));
		content=appendContent(content, tmpcontent);
	}

	content=appendContent(content, "\txor %rdx, %rdx\n");
	content=appendContent(content, "\tidiv %rbx\n");
	content=appendContent(content, "\tpush %rdx\n\n");		// rdx is reminder of di
	return content;
}

char* conditioner(char* content, char* op1, char* op2, int jumpPoint) {
	char tmpcontent[100];
	if(op1[0] == '\0')
		content=appendContent(content, "\tpop %rbx\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rbx\n", atoi(op1));
		content=appendContent(content, tmpcontent);
	}
	if(op2[0] == '\0')
		content=appendContent(content, "\tpop %rax\n");
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rax\n", atoi(op2));
		content=appendContent(content, tmpcontent);
	}

	content=appendContent(content, "\tcmp %rax, %rbx\n");
	sprintf(tmpcontent, "\tjnz jump%d\n\n", jumpPoint);
	content=appendContent(content, tmpcontent);
	return content;
}

char* ifbuilder(char *content, int jumpPoint) {
	char tmpcontent[100];
	sprintf(tmpcontent, "jump%d:\n", jumpPoint);
	content=appendContent(content, tmpcontent);
	return content;
}

char* loopbuilder(char* content, char* op1, int loopPoint) {
	char tmpcontent[100];

	if(op1[0] == '\0'){
		content=appendContent(content, "\tpop %rcx\n");
	}
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rcx\n", atoi(op1));
		content=appendContent(content, tmpcontent);
	}
	content=appendContent(content, "\txor %rax, %rax\n");
	content=appendContent(content, "\tcmp %rax, %rcx\n");
	sprintf(tmpcontent, "\tje endloop%d\n\n", loopPoint);
	content=appendContent(content, tmpcontent);
	sprintf(tmpcontent, "loop%d:\n", loopPoint);
	content=appendContent(content, tmpcontent);
	content=appendContent(content, "\tpush %rcx\n");
	return content;
}

char* looptailbuilder(char *content, int loopPoint) {
	char tmpcontent[100];
	content=appendContent(content, "\tpop %rcx\n");
	content=appendContent(content, "\tdec %rcx\n");
	sprintf(tmpcontent, "\tjnz loop%d\n\n", loopPoint);
	content=appendContent(content, tmpcontent);
	sprintf(tmpcontent, "endloop%d:\n", loopPoint);
	content=appendContent(content, tmpcontent);
	return content;
}

char* loopTObuilder(char *content, char* op1,int loopPoint) {
	char tmpcontent[100];

	if(op1[0] == '\0'){
		content=appendContent(content, "\tpop %rcx\n");
	}
	else {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rcx\n", atoi(op1));
		content=appendContent(content, tmpcontent);
	}
	content=appendContent(content, "\tpop %rbx\n");
	content=appendContent(content, "\tpop %rax\n");
	content=appendContent(content, "\tsub %rbx,%rax\n");
	content=appendContent(content, "\tinc %rax\n");
	content=appendContent(content, "\tpush %rax\n");


	content=appendContent(content, "\txor %rax, %rax\n");
	content=appendContent(content, "\tcmp %rax, %rcx\n");
	sprintf(tmpcontent, "\tje endloop%d\n\n", loopPoint);
	content=appendContent(content, tmpcontent);
	sprintf(tmpcontent, "loop%d:\n", loopPoint);
	content=appendContent(content, tmpcontent);
	content=appendContent(content, "\tpush %rcx\n");
	return content;

}

char* printAstr(char* content, int op1) {
	char tmpcontent[100];
	content=appendContent(content, "\tpush %rax\n");
	content=appendContent(content, "\tpush %rbx\n");
	content=appendContent(content, "\tpush %rcx\n");
	sprintf(tmpcontent, "\tmov $str%d, %%rdi\n", op1);
	content=appendContent(content, tmpcontent);
	content=appendContent(content, "\tcall printf\n");
	content=appendContent(content, "\tpop %rcx\n");
	content=appendContent(content, "\tpop %rbx\n");
	content=appendContent(content, "\tpop %rax\n\n");
	return content;
}

char* printer(char* content, char* op1, bool hex) {
	char tmpcontent[100];
	if(op1[0] == '\0') 
		content=appendContent(content, "\tpop %rax\n");

	content=appendContent(content, "\tpush %rax\n");
	content=appendContent(content, "\tpush %rbx\n");
	content=appendContent(content, "\tpush %rcx\n");
	if(hex)
		content=appendContent(content, "\tmov $printx, %rdi\n");
	else 
		content=appendContent(content, "\tmov $print, %rdi\n");

	if(op1[0] != '\0') {
		sprintf(tmpcontent, "\tmov -%d(%%rbp), %%rax\n", atoi(op1));
		content=appendContent(content, tmpcontent);
	}

	content=appendContent(content, "\tmov %rax, %rsi\n");
	content=appendContent(content, "\txor %rax, %rax\n");
	content=appendContent(content, "\tcall printf\n");
	content=appendContent(content, "\tpop %rcx\n");
	content=appendContent(content, "\tpop %rbx\n");
	content=appendContent(content, "\tpop %rax\n\n");
	return content;
}

